import React from 'react';
import './App.css';
import GroupAComponent from './Components/GroupA';
import GroupBComponent from './Components/GroupB';
import GroupCComponent from './Components/GroupC';
import GroupDComponent from './Components/GroupD';

const App: React.FC = () => {
  return (
    <>
      <div className="block" id="groupA">
        <GroupAComponent />
      </div>
      <div className="block" id="groupB">
        <GroupBComponent />
      </div>
      <div className="block" id="groupC">
        <GroupCComponent />
      </div>
      <div className="block" id="groupD">
        <GroupDComponent />
      </div>
    </>
  );
};

export default App;
