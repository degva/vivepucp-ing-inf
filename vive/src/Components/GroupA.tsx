import * as React from 'react';
import triangulo from '../imgs/triangulo.png';

export interface GroupAProps {}

export interface GroupAState {
  ladoA: string | number;
  ladoB: string | number;
  ladoC: string | number;
  resultado?: number;
}

export default class GroupAComponent extends React.Component<
  GroupAProps,
  GroupAState
> {
  constructor(props: GroupAProps) {
    super(props);
    this.state = {
      ladoA: '',
      ladoB: '',
      ladoC: ''
    };
  }

  areaDelTriangulo = () => {
    const ladoA = this.state.ladoA as number;
    const ladoB = this.state.ladoB as number;
    const ladoC = this.state.ladoC as number;

    // Ingresa aquí tu código!
    const resultado = (((ladoA*ladoA) + ((ladoB/2)*(ladoB/2))/2)*ladoB)/2 ;

    // No modifiques después de esta línea!

    this.setState(pS => ({
      ...pS,
      resultado
    }));
  };

  handleChange = (thing: string) => ({
    target: { value }
  }: {
    target: { value: string };
  }) => {
    this.setState(
      pS => ({
        ...pS,
        [thing]: value
      }),
      this.areaDelTriangulo
    );
  };

  public render() {
    return (
      <>
        <div className="blocky">
          <h1>Área</h1>
          <input
            placeholder="Lado A"
            type="number"
            min="0"
            value={this.state.ladoA}
            onChange={this.handleChange('ladoA')}
          />
          <br />
          <input
            placeholder="Lado B"
            type="number"
            min="0"
            value={this.state.ladoB}
            onChange={this.handleChange('ladoB')}
          />
          <br />
          <input
            placeholder="Lado C"
            type="number"
            min="0"
            value={this.state.ladoC}
            onChange={this.handleChange('ladoC')}
          />
          <br />
          <img
            style={{ right: '0', position: 'relative', top: '0' }}
            src={triangulo}
            alt="Triangulo"
          />
          <div className="result">
            {this.state.resultado ? (
              <p>El área del triangulo es {this.state.resultado}</p>
            ) : null}
          </div>
        </div>
      </>
    );
  }
}
