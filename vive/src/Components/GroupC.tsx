import * as React from 'react';
import trapecio from '../imgs/trapecio.png';

export interface GroupCProps {}

export interface GroupCState {
  ladoA: string | number;
  ladoB: string | number;
  altura: string | number;
  resultado?: number;
}

export default class GroupCComponent extends React.Component<
  GroupCProps,
  GroupCState
> {
  constructor(props: GroupCProps) {
    super(props);
    this.state = {
      ladoA: '',
      ladoB: '',
      altura: ''
    };
  }

  areaDelTrapecio = () => {
    const ladoA = this.state.ladoA as number;
    const ladoB = this.state.ladoB as number;
    const altura = this.state.altura as number;

    // Ingresa aquí tu código!
    const resultado = (ladoA / ladoB) * altura;

    // No modifiques después de esta línea!

    this.setState(pS => ({
      ...pS,
      resultado
    }));
  };

  handleChange = (thing: string) => ({
    target: { value }
  }: {
    target: { value: string };
  }) => {
    this.setState(pS => ({ ...pS, [thing]: value }), this.areaDelTrapecio);
  };

  public render() {
    return (
      <>
        <div className="blocky">
          <h1>Área de un trapecio</h1>
          <input
            placeholder="Lado paralelo A"
            type="number"
            min="0"
            value={this.state.ladoA}
            onChange={this.handleChange('ladoA')}
          />
          <br />
          <input
            placeholder="Lado paralelo B"
            type="number"
            min="0"
            value={this.state.ladoB}
            onChange={this.handleChange('ladoB')}
          />
          <br />
          <input
            placeholder="Altura H"
            type="number"
            min="0"
            value={this.state.altura}
            onChange={this.handleChange('altura')}
          />
          <br />
          <img
            style={{ right: '0', position: 'relative', top: '0' }}
            src={trapecio}
            alt="Trapecio"
          />
          <div className="result">
            {this.state.resultado ? (
              <p>El área del trapecio es {this.state.resultado}</p>
            ) : null}
          </div>
        </div>
      </>
    );
  }
}
