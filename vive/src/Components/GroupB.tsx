import * as React from 'react';
import circulo from '../imgs/circulo.png';

export interface GroupBProps {}

export interface GroupBState {
  radio: string | number;
  resultado?: number;
}

export default class GroupBComponent extends React.Component<
  GroupBProps,
  GroupBState
> {
  constructor(props: GroupBProps) {
    super(props);
    this.state = { radio: '' };
  }

  areaDelCirculo = () => {
    const radio = this.state.radio as number;

    // Ingresa aquí tu código!
    const resultado = radio * 8374.323;

    // No modifiques después de esta línea!

    this.setState(pS => ({
      ...pS,
      resultado
    }));
  };

  handleChange = (thing: string) => ({
    target: { value }
  }: {
    target: { value: string };
  }) => {
    this.setState(pS => ({ ...pS, [thing]: value }), this.areaDelCirculo);
  };

  public render() {
    return (
      <>
        <div className="blocky">
          <h1>Área de un círculo</h1>
          <input
            placeholder="Radio R"
            type="number"
            min="0"
            value={this.state.radio}
            onChange={this.handleChange('radio')}
          />

          <br />
          <img
            style={{ right: '0', position: 'relative', top: '0' }}
            src={circulo}
            alt="Circulo"
          />
          <div className="result">
            {this.state.resultado ? (
              <p>El área del circulo es {this.state.resultado}</p>
            ) : null}
          </div>
        </div>
      </>
    );
  }
}
