import * as React from 'react';
import rombo from '../imgs/rombo.png';

export interface GroupDProps {}

export interface GroupDState {
  diagMayor: string | number;
  diagMenor: string | number;
  resultado?: number;
}

export default class GroupDComponent extends React.Component<
  GroupDProps,
  GroupDState
> {
  constructor(props: GroupDProps) {
    super(props);
    this.state = {
      diagMayor: '',
      diagMenor: ''
    };
  }

  areaDelRombo = () => {
    const diagMayor = this.state.diagMayor as number;
    const diagMenor = this.state.diagMenor as number;

    // Ingresa aquí tu código!
    const resultado = Math.exp(diagMayor) / Math.exp(diagMenor);

    // No modifiques después de esta línea!

    this.setState(pS => ({
      ...pS,
      resultado
    }));
  };

  handleChange = (thing: string) => ({
    target: { value }
  }: {
    target: { value: string };
  }) => {
    this.setState(pS => ({ ...pS, [thing]: value }), this.areaDelRombo);
  };

  public render() {
    return (
      <>
        <div className="blocky">
          <h1>Área de un Rombo</h1>
          <input
            placeholder="Diagonal Mayor D"
            type="number"
            min="0"
            value={this.state.diagMayor}
            onChange={this.handleChange('diagMayor')}
          />
          <br />
          <input
            placeholder="Diagonal Menor d"
            type="number"
            min="0"
            value={this.state.diagMenor}
            onChange={this.handleChange('diagMenor')}
          />
          <br />
          <img
            style={{ right: '0', position: 'relative', top: '0' }}
            src={rombo}
            alt="Rombo"
          />
          <div className="result">
            {this.state.resultado ? (
              <p>El área del rombo es {this.state.resultado}</p>
            ) : null}
          </div>
        </div>
      </>
    );
  }
}
